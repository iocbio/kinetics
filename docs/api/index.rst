.. proovikeneeetika documentation master file, created by
   sphinx-quickstart on Wed Dec 11 12:39:19 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Kinetics
============================================
.. toctree::
   :maxdepth: 6

   index
   Return to Main Documentation <https://iocbio.gitlab.io/kinetics/>
	      
.. _base codes:

.. module:: iocbio.kinetics.constants
   
      
.. module:: iocbio.kinetics.global_vars
   

############
Applications
############

===================
App module
===================

---------------------
iocbio.kinetics.app
---------------------

.. automodule:: iocbio.kinetics.app.__init__
   :members:

-------------------------
iocbio.kinetics.app.fetch
-------------------------
      
.. automodule:: iocbio.kinetics.app.fetch
   :members:

----------------------------------
iocbio.kinetics.app.fetch_repeated
----------------------------------

.. automodule:: iocbio.kinetics.app.fetch_repeated
   :members:

--------------------------
iocbio.kinetics.app.banova
--------------------------

.. automodule:: iocbio.kinetics.app.banova
   :members:
  

.. _codes:

################
Core
################

.. _constants:

=========
Constants
=========

.. automodule:: iocbio.kinetics.constants
   :members:

.. _calc:

======================
Calc: Analysis modules
======================

---------------------
iocbio.kinetics.calc
---------------------

.. automodule:: iocbio.kinetics.calc.__init__
   :members:

-------------------------
iocbio.kinetics.calc.bump
-------------------------

.. automodule:: iocbio.kinetics.calc.bump
   :members:

-----------------------------
iocbio.kinetics.calc.composer
-----------------------------

.. automodule:: iocbio.kinetics.calc.composer
   :members:       

----------------------------
iocbio.kinetics.calc.current
----------------------------

.. automodule:: iocbio.kinetics.calc.current
   :members:      

-------------------------------
iocbio.kinetics.calc.explin_fit
-------------------------------

.. automodule:: iocbio.kinetics.calc.explin_fit
   :members:

----------------------------
iocbio.kinetics.calc.generic
----------------------------

.. automodule:: iocbio.kinetics.calc.generic
   :members:

---------------------------
iocbio.kinetics.calc.linreg
---------------------------

.. automodule:: iocbio.kinetics.calc.linreg
   :members:

---------------------------------
iocbio.kinetics.calc.mean_med_std
---------------------------------

.. automodule:: iocbio.kinetics.calc.mean_med_std
   :members:

-----------------------
iocbio.kinetics.calc.mm
-----------------------

.. automodule:: iocbio.kinetics.calc.mm
   :members:

-----------------------------
iocbio.kinetics.calc.peakarea
-----------------------------

.. automodule:: iocbio.kinetics.calc.peakarea
   :members:


-----------------------
iocbio.kinetics.calc.xy
-----------------------

.. automodule:: iocbio.kinetics.calc.xy
   :members:
      
--------------------------------
iocbio.kinetics.calc.respiration
--------------------------------

.. automodule:: iocbio.kinetics.calc.respiration
   :members:


.. _handler:

==========
Handler
==========

.. automodule:: iocbio.kinetics.handler.__init__
   :members:

------------------------------------------
iocbio.kinetics.handler.experiment_generic
------------------------------------------

.. automodule:: iocbio.kinetics.handler.experiment_generic
   :members:

---------------------------
iocbio.kinetics.handler.roi
---------------------------

.. automodule:: iocbio.kinetics.handler.roi
   :members: 

      
.. _gui:

==========
Gui
==========

.. automodule:: iocbio.kinetics.gui
   :members:

.. automodule:: iocbio.kinetics.gui.experiment_plot
   :members:

.. automodule:: iocbio.kinetics.gui.mainwindow
   :members:


.. automodule:: iocbio.kinetics.gui.roi_list
   :members:

.. automodule:: iocbio.kinetics.gui.stats_widget
   :members:

.. automodule:: iocbio.kinetics.gui.xy_plot
   :members:

     

.. _io:

==========
IO
==========

---------------------
iocbio.kinetics.io
---------------------

.. automodule:: iocbio.kinetics.io
   :members:

------------------------
iocbio.kinetics.io.data
------------------------

.. automodule:: iocbio.kinetics.io.data
   :members:

------------------------
iocbio.kinetics.io.db
------------------------

.. automodule:: iocbio.kinetics.io.db
   :members:

--------------------------
iocbio.kinetics.io.modules
--------------------------

.. automodule:: iocbio.kinetics.io.modules
   :members:      

      
.. _modules:

###############
Example Modules
###############

.. _sysbio:

======
Sysbio
======



.. _confocal_catransient:

---------------------
Confocal-catransient
---------------------

.. automodule:: iocbio.kinetics.modules.sysbio.confocal_catransient.analyzer
   :members:

.. automodule:: iocbio.kinetics.modules.sysbio.confocal_catransient.reader
   :members:


.. _electrophysiology:

---------------------
Electrophysiology
---------------------
      
.. automodule:: iocbio.kinetics.modules.sysbio.electrophysiology.elec_current_analyzer
   :members:
      
.. automodule:: iocbio.kinetics.modules.sysbio.electrophysiology.elec_current_fluo_analysers
   :members:
      
.. automodule:: iocbio.kinetics.modules.sysbio.electrophysiology.elec_fluo_analyzer
   :members:
      
.. automodule:: iocbio.kinetics.modules.sysbio.electrophysiology.reader
   :members:

.. _mechanics:

---------------------
Mechanics
---------------------
.. automodule:: iocbio.kinetics.modules.sysbio.mechanics.experiment_mechanics
   :members:

.. automodule:: iocbio.kinetics.modules.sysbio.mechanics.mech_ufreqcasl
   :members:


.. automodule:: iocbio.kinetics.modules.sysbio.mechanics.mech_ufreqcasl_lowhigh
   :members:


.. automodule:: iocbio.kinetics.modules.sysbio.mechanics.reader
   :members:

  

.. _misc:

---------------------
Misc
---------------------
.. automodule:: iocbio.kinetics.modules.sysbio.misc.set_prepid
   :members:

.. _respiration:

---------------------
Respiration
---------------------
.. automodule:: iocbio.kinetics.modules.sysbio.respiration.analyzer_post
   :members:

.. automodule:: iocbio.kinetics.modules.sysbio.respiration.analyzer_primary
   :members:

.. automodule:: iocbio.kinetics.modules.sysbio.respiration.experiment_strathkelvin
   :members:

.. automodule:: iocbio.kinetics.modules.sysbio.respiration.reader
   :members:
      

.. _spectro:

---------------------
Spectro
---------------------
.. automodule:: iocbio.kinetics.modules.sysbio.spectro.analyzer_primary
   :members:

.. automodule:: iocbio.kinetics.modules.sysbio.spectro.experiment_spectro
   :members:


.. automodule:: iocbio.kinetics.modules.sysbio.spectro.reader
   :members:





      


Indices and tables
==================


* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

