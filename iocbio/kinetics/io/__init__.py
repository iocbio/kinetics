"""Classes for handling database, data transport from reader to analyzer, and loading of modules

I/O modules are responsible for communication with the database,
representing experimental data allowing it to be transferred from the
reader to the analyzer and handling loading of the modules.

"""
