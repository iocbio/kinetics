# Modules for experiments in Lab of Systems Biology at TalTech

These modules are developed for different types of experiments in
Laboratory of Systems Biology, Department of Cybernetics, School of
Science, Tallinn University of Technology (https://sysbio.ioc.ee).
